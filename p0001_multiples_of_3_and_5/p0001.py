# This Python script will print the sum of all multiples of 3 and 5, up to 1000

if __name__ == '__main__':
    i = 0
    sum_3_5 = 0
    while i < 1000:
        if i % 3 == 0 or i % 5 == 0:
            sum_3_5 += i
        i += 1
    print(sum_3_5)
