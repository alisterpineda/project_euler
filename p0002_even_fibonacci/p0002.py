# This Python script will print out the sum of all even Fibonacci numbers up to 4 million.
# Fibonacci sequence in this case starts at 1 + 2 + ...


class Fibonacci(object):
    """
    F_n = F_n-1 + F_n-2
    """

    def __init__(self, seed0: int, seed1: int):
        self.f_n_minus_2 = seed0
        self.f_n_minus_1 = seed1

    def next(self) -> int:
        val = self.f_n_minus_2 + self.f_n_minus_1

        self.f_n_minus_2 = self.f_n_minus_1
        self.f_n_minus_1 = val

        return val


if __name__ == '__main__':
    fib = Fibonacci(1, 2)

    sum_even_fib = fib.f_n_minus_1

    curr = fib.f_n_minus_1
    while curr < 4_000_000:
        curr = fib.next()
        if curr % 2 == 0:
            sum_even_fib += curr

    print(sum_even_fib)
